import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="bg-dark text-white">
                <h6>@Persona App 2020</h6>
            </div>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)